#!/usr/bin/env python
import socket, ConfigParser, time, logs, sys, os, threading, json
import paho.mqtt.client as mqtt

### Get run mode
if len(sys.argv) > 1:
	runmode = sys.argv[1]
elif len(sys.argv) == 1:
	runmode = 'normal'

logger = logs.setup_logger('rfid2mqtt')
logger.warning("Program Started...")
starttime=time.time()

### Functions start here

def network_check():
	import socket
	loop = 1
        while loop == 1:
		try:
			host = socket.gethostbyname(config['mqtt_broker_hostname'])
 			s = socket.create_connection((host, config['mqtt_broker_port']), 2)
			logger.warning("Network checks passed!, '%s' is reachable." % config['mqtt_broker_hostname'])
			loop = 0
		except Exception:
			logger.warning("Network checks failed!, stalling until MQTT Broker '%s' becomes reachable..." % config['mqtt_broker_hostname'])
			time.sleep(5)

def config_load():
	global config
	global sockets
	config = {}
	config_file = ConfigParser.RawConfigParser()
	config_file.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'config.cfg'))
	config.update({
	'hostid':config_file.get('host', 'id'),
	'mqtt_broker_hostname':config_file.get('mqtt_broker', 'hostname'),
	'mqtt_broker_port':config_file.getint('mqtt_broker', 'port'),
	'mqtt_broker_username':config_file.get('mqtt_broker', 'username'),
	'mqtt_broker_pass':config_file.get('mqtt_broker', 'password'),
	'mqtt_topic':config_file.get('mqtt_broker', 'topic')
	})
	sockets = {}
	for each_section in config_file.sections():
		if each_section.startswith('alien_reader'):
			sockets.update({each_section:{}})
			for (key, value) in config_file.items(each_section):
				sockets[each_section].update({key:value})

def socket_open(ip, port):
	logger.warning("Attemping to bind to IP '%s' and port '%s'." % (ip, port))
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#Bind socket to local host and port
	try:
		s.bind((ip, int(port)))
	except s.error as msg:
		logger.warning("Bind failed. Error Code '%s' Message '%s'" % msg[0],  msg[1])
		sys.exit()
	#Start listening on socket
	s.listen(10)
	logger.warning("Socket bind complete.")
	return s

### MQTT funcionts here
def on_connect(client, userdata, flags, rc):
	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	if rc == 0:
		logger.warning("Succesfully connected to MQTT broker, starting subscriptions if any.")
	else:
		logger.warning("Connection to MQTT broker failed. rc='%s' " % s)

def on_publish(client, userdata, mid):
	logger.debug("PUBLISH 'm%s' completed." % mid)

def on_message(client, userdata, msg):
	logger.warning("Received Publish '%s' with a value of '%s'." % (msg.topic, msg.payload))
	#print "Received Publish '"+msg.topic+"' with a value of '"+msg.payload+"'."
	#if msg.topic == 'dispatcher/'+location+'/'+sublocation+'/'+deviceid+'/lights':
		#if str(msg.payload) == 'on':

def on_disconnect(client, userdata, rc):
	if rc != 0:
		logger.warning("Unexpected MQTT Broker disconnection. Will auto-reconnect.")

def on_log(mosq, obj, level, string):
	logger.debug(string)

### Classes / functions start here
class mqtt_connect(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		return
	def run(self):
		global mqttc
		mqttc = mqtt.Client(config['hostid'])
		mqttc.username_pw_set(config['mqtt_broker_pass'], config['mqtt_broker_pass'])
		mqttc.connect(config['mqtt_broker_hostname'], config['mqtt_broker_port'], 80)
		mqttc.on_connect = on_connect
		mqttc.on_message = on_message
		mqttc.on_publish = on_publish
		mqttc.on_disconnect = on_disconnect
		# Uncomment here to enable MQTT debug messages
		#mqttc.on_log = on_log
		mqttc.loop_start()

class socket_listener(threading.Thread):
	def __init__(self, reader, location, sublocation, socket):
		threading.Thread.__init__(self)
		self.reader = reader
		self.location = location
		self.sublocation = sublocation
		self.socket = socket
		return
	def run(self):
		while 1:
			#wait to accept a connection - blocking call
			connection, addr = self.socket.accept()
			prfidt = publish_rfid(self.reader, self.location, self.sublocation, connection)
			prfidt.daemon = True
			prfidt.start()
			logger.warning("Started a publish_rfid thread for Alien Reader '%s' connected at socket '%s':'%s'." % (reader, addr[0], addr[1]))
			if runmode == 'debug':
				print "Started a publish_rfid thread for socket '%s':'%s'." % (addr[0], addr[1])
		self.socket.close()

class publish_rfid(threading.Thread):
	def __init__(self, reader, location, sublocation, connection):
		threading.Thread.__init__(self)
		self.reader = reader
		self.location = location
		self.sublocation = sublocation
		self.buffer_size = 100
		self.connection = connection
		return
	def run(self):
		while 1:
			base_payload = {'reader':self.reader, 'location':self.location, 'sublocation':self.sublocation}
			rfid_tag_payload = {}
			payload = {}
			readout = self.connection.recv(self.buffer_size)
			### For debugging porpuses we print all readouts when running in debug mode
			if runmode == 'debug':
				print readout
			if not readout or readout[0][0] != 'E' or len(readout) < 36:
				continue
			else:
				readout = readout.split(",")
			tag = readout[0].replace(" ", "")
			antenna = readout[1]
			#print "About to publush the following: reader='%s', antenna='%s', tag='%s'" % (self.reader, antenna, tag)
			rfid_tag_payload = {'tag':tag,'antenna':antenna}
			if len(rfid_tag_payload) >= 1:
					payload.update(base_payload)
					payload.update(rfid_tag_payload)
					### Finally pass the container to the MQTT thread for publishing
					mqttc.publish(config['mqtt_topic'], payload=json.dumps(payload))
					if runmode == 'debug':
						print config['mqtt_topic']+str(payload)

if __name__ == "__main__":
	print "Rfid2mqtt is spinning. Check /var/log/rfid2mqtt.log for more details..."
	# Load config from file config.cfg
	config_load()
	# Perfrom network checks and wait there until we can reach the MQTT broker
	network_check()
	try:
		# Start MQTT thread here 
		thread0 = mqtt_connect()
		thread0.daemon = True
		thread0.start()
		logger.warning("Started mqtt_connect thread...")
		if runmode == 'debug':
			print "Started mqtt_connect thread..."
		# Start socket listener threads here
		for reader, attributes in sockets.iteritems():
			socket = socket_open(attributes['ip'], attributes['port'])
			listener = socket_listener(reader, attributes['location'], attributes['sublocation'], socket)
			listener.daemon = True
			listener.start()
			logger.warning("Started a socket_listener thread for Alien Reader '%s'." % reader)
			if runmode == 'debug':
				print "Started a socket_listener thread for Alien Reader '%s'..." % reader

	except KeyboardInterrupt:
		logger.exception("Got exception on main handler:")
		if runmode == 'debug':
			print "Got exception on main handler:"
		mqttc.loop_stop()
		mqttc.disconnect()

	except:
		logger.exception("Other error or exception occurred!")
		if runmode == 'debug':
			print "Other error or exception occurred!"
		mqttc.loop_stop()
		mqttc.disconnect()

	while True:
		time.sleep(1)
		pass
